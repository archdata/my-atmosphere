#! /usr/bin/python3
import os
import sys
import glob
import json
import time
import xbee
import serial
import bitstring
import mysql.connector

import logging
import logging.handlers
import argparse

# Deafults
LOG_FILENAME = "/tmp/myatmosphere.log"
LOG_LEVEL = logging.INFO  # Could be e.g. "DEBUG" or "WARNING"

# Define and parse command line arguments
parser = argparse.ArgumentParser(description="MyAtmosphere Python service")
parser.add_argument("-l", "--log", help="file to write log to (default '" + LOG_FILENAME + "')")

# If the log file is specified on the command line then override the default
args = parser.parse_args()
if args.log:
    LOG_FILENAME = args.log

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger(__name__)
# Set the log level to LOG_LEVEL
logger.setLevel(LOG_LEVEL)
# Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
# Format each log message like this
formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
# Attach the formatter to the handler
handler.setFormatter(formatter)
# Attach the handler to the logger
logger.addHandler(handler)

# Make a class we can use to capture stdout and sterr in the log
class MyLogger(object):
    def __init__(self, logger, level):
        """Needs a logger and a logger level."""
        self.logger = logger
        self.level = level

    def write(self, message):
        # Only log if there is a message (not just a new line)
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())

# Replace stdout with logging to file at INFO level
sys.stdout = MyLogger(logger, logging.INFO)
# Replace stderr with logging to file at ERROR level
sys.stderr = MyLogger(logger, logging.ERROR)

class Database():

    def __init__(self, user=None, password=None, host=None, database=None):

        if user is None:
            sys.exit('No database user specified')
        else:
            self.user = user

        if password is None:
            sys.exit('No database password specified')
        else:
            self.password = password

        if host is None:
            sys.exit('No database host address specified')
        else:
            self.host = host

        if database is None:
            sys.exit('No database specified')
        else:
            self.database = database

        self.mysql_connection = mysql.connector.connect(user=self.user, password=self.password, host=self.host, database=self.database)
        self.cursor = self.mysql_connection.cursor()

    def close_mysql_connection(self):
        self.cursor.close()
        self.mysql_connection.close()

    def write_data(self, sql_statement, data_readings):

        self.cursor.execute(sql_statement, data_readings)
        self.mysql_connection.commit()

    def check_for_table(self, table_name):

        check_table_sql = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'sensors' AND table_name = %s;"
        self.cursor.execute(check_table_sql, [table_name])

        result = self.cursor.fetchone()

        if result[0] >= 1:
            # there is a table named "table_name"
            return True
        else:
            # there are no tables named "table_name"
            return False

    def sensors_to_database(self, data_list):

        # print (data_list)
        if data_list is not None:
            sensor_info = data_list[0]
            data = data_list[1]

            unpack_commands = sensor_info['sensor_info']['unpack_commands']
            sql_statement = sensor_info['database_query']
            database_table = sensor_info['sensor_info']['name']

            if (self.check_for_table(database_table)):
                # Unpack the data from the incoming serial data
                values = data.unpack(unpack_commands)
                self.write_data(sql_statement, values)


class XBeeSerial():
    """
    Connects to the serial port and gathers the incoming data.
    Works with both XBee series 1 and series 2. Series 2 is default but can be changed when initializing the class
    A serial port will need to be set when initializing the class or failure ensues

    """
    def __init__(self, serial_port=None, xbee_series=None, baudrate=None):

        if xbee_series is None:
            self.xbee_series = 2
        else:
            self.xbee_series = xbee_series

        if serial_port is None:
            sys.exit('No serial port spcified!')
        else:
            self.serial_port = serial_port

        if baudrate is None:
            self.baudrate = 9600
        else:
            self.baudrate = baudrate

        try:
            self.serial_connection = serial.Serial(self.serial_port, self.baudrate)
        except (SerialException) as e:
            logger.info(e)

        # Works with both Xbee series, defaults to Series 2
        if self.xbee_series == 2:
            self.xbee = xbee.ZigBee(self.serial_connection, escaped=True)
        else:
            self.xbee = xbee.XBee(self.serial_connection, escaped=True)

    def aquire_serial_data(self):

        if self.serial_connection.isOpen():

            self.serial_connection.flushInput()

            response = self.xbee.wait_read_frame()
            data = bitstring.BitStream(bytes=response['rf_data'])
            return data

    def close_serial_port(self):
        self.serial_connection.close()


class Sensors():

    def __init__(self):
        pass

    def get_sensor_address(self, data):

        address_bytes = data.peek(16)
        address = address_bytes.unpack('uint')
        # Address is a list of one interger value - return just the interger
        return address[0]

    def get_sensor_info(self, sensor_address):

        sensor_definition_path = os.path.dirname(os.path.realpath(__file__)) + '/sensors/'
        sensor_definition_file = 'sensor-' + str(sensor_address) + '.json'
        json_files =  sensor_definition_path + '*.json'
        names = [os.path.basename(x) for x in glob.glob(json_files)]

#         print(names)
        if sensor_definition_file in names:

            json_file = open(sensor_definition_path + sensor_definition_file)
            sensor_info = json.load(json_file)

            if sensor_info['enabled'].lower() == "true":
                return sensor_info
            else:
                return False

        else:
            return False

    def calc_sensor_data(self, data):

        # Get the address of the sensor
        address = self.get_sensor_address(data)

        # Get the json file containing the specs and instrutions for the sensor
        sensor_definition = self.get_sensor_info(address)

        # Remove the sensor address bits from the beginning of the data packet before sending the data on
        del data[:16]

        # If the sensor definition file exists keep going.
        if sensor_definition:
            return [sensor_definition, data]


def main():
    """ Main function """
    # This is important for starting this script at boot, wait for hardware to become available.
    time.sleep(30)

    try:
        database = Database('root', 'scotish1', '127.0.0.1', 'sensors')
        ser = XBeeSerial('/dev/ttyUSB0', 1, 9600)
        sensors = Sensors()

    except BaseException as e:
        pass

    while True:

        try:

            aquired_data = ser.aquire_serial_data()
#             print (aquired_data)
            sensor_data = sensors.calc_sensor_data(aquired_data)
#             print (sensor_data)
            database.sensors_to_database(sensor_data)

        except (KeyboardInterrupt, SystemExit):
            database.close_mysql_connection()
            ser.close_serial_port()
            sys.exit(0)


if __name__ == '__main__':

    main()
